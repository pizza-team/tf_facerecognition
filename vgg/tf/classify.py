import skimage.io
from skimage.transform import resize
import tensorflow as tf
import numpy as np

from vgg.tf.download_img import download_image

image_url = \
"https://media.licdn.com/mpr/mpr/shrinknp_200_200/p/1/000/292/136/152654d.jpg"

download_image(image_url)

log_path = "./logs"

image = skimage.io.imread('/Users/marcouras/PycharmProjects/tensorface/vgg/tf/source.jpg') / 255.0
image = resize(image, (224, 224))
image = image.reshape((1, 224, 224, 3))

content = tf.Variable(image, dtype=tf.float32)
with open("/Users/marcouras/PycharmProjects/tensorface/vgg/tf/vggface16.tfmodel", mode='rb') as f:
    fileContent = f.read()


graph_def = tf.GraphDef()
graph_def.ParseFromString(fileContent)
tf.import_graph_def(graph_def, input_map={"images": content})
graph = tf.get_default_graph()

writer = tf.summary.FileWriter(logdir=log_path, graph=tf.get_default_graph())


probs = graph.get_tensor_by_name("import/prob:0")
conv5_3_relu = graph.get_tensor_by_name("import/conv5_3/Relu:0")

with tf.Session() as sess:
    sess.run(tf.global_variables_initializer())
    # computing class probabilities
    probss = sess.run(probs)
    probss = np.array(probss).ravel()
    print ("\nPosition = {}".format(np.argmax(probss)))
    probability = probss.max()

with open('/Users/marcouras/PycharmProjects/tensorface/vgg/dataset_vgg/nomi.txt', 'r') as f:
    names = f.readlines()
    name = names[np.argmax(probss)]

print("Name = {}".format(name))
print("Probability = {}".format(probability))
writer.close()
